# Chemins
root_dir="/builds/$GITLAB_USER_LOGIN/constantin_tp5"
env_dir="$root_dir/prod-env"
terraform_dir="$env_dir/terraform"
ansible_dir="$env_dir/ansible"

cd $terraform_dir
prod_ip=$(terraform output prod_cluster_external_ip | sed 's/"//g')
user=$(terraform output cluster_user | sed 's/"//g')

# Génération de l'inventaire avec les adresses IP
echo "[prod]"
echo $prod_ip ansible_user=$user
