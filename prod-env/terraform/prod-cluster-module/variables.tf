variable "network_id_prod" {
  type = string
}

variable "subnetwork_id_prod" {
  type = string
}

variable "user" {
  default     = "constantin.formation2"
  description = "Nom de l'utilisateur"
  type        = string
}
