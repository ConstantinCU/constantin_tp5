# instance vm prod

resource "google_container_cluster" "tp5-cluster" {
  name               = "tp5-cluster"
  location           = "us-central1"
  project            = "tp5-logging"
  remove_default_node_pool = true
  initial_node_count = 1
  network    = var.network_id_prod
  subnetwork = var.subnetwork_id_prod
  networking_mode = "VPC_NATIVE"

  ip_allocation_policy {
    cluster_secondary_range_name = "k8s-pod-range"
  }

  private_cluster_config {
    enable_private_nodes = true
    enable_private_endpoint = false
  }

  # Cluster properties
  node_config {
    preemptible  = true
    tags         = ["nodes"]
    machine_type = g1-small
    disk_size_gb = 20
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring"
    ]

    # Ajout de la clé SSH
    metadata = {
      "ssh-keys" = "${var.user}:${file("~/.ssh/id_rsa.pub")}"
    }
  }
}
