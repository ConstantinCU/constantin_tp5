variable "project_id" {}

variable "credentials_file" {}

variable "user" {
  default     = "constantin.formation2"
  description = "Nom de l'utilisateur"
  type        = string
}

variable "region" {
  default = "us-central1"
  type    = string
}

variable "zone" {
  default = "us-central1-a"
  type    = string
}
