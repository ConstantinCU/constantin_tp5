# MODULES
module "network" {
  source = "./prod-network-module"
}

module "prod_vm" {
  source        = "./prod-cluster-module"
  network_id_prod    = module.network.network_id_prod
  subnetwork_id_prod = module.network.subnetwork_id_prod
}
