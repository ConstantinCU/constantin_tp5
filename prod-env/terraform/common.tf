# PROVIDER
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.20.0"
    }
  }
  backend "gcs" {
    bucket = "tp5-bucket-prod"
    prefix = "terraform/state"
  }
}

provider "google" {
  credentials = file(var.credentials_file)
  project     = var.project_id
  region      = var.region
}
