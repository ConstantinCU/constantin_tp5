# réseau pour prod
resource "google_compute_network" "network_prod" {
  name                    = "network-prod"
  auto_create_subnetworks = false
}

# sous-réseau pour prod
resource "google_compute_subnetwork" "subnetwork_prod" {
  name          = "subnetwork-prod"
  ip_cidr_range = "10.0.0.0/18"
  region        = "us-east1"
  network       = google_compute_network.network_prod.id

  secondary_ip_range = {
    range_name = "k8s-pod-range"
    ip_cidr_range = "10.48.0.0/14"
  }
}

# firewall ssh
resource "google_compute_firewall" "firewall_ssh_prod" {
  name    = "allow-ssh-prod"
  network = google_compute_network.network_prod.self_link

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["server-prod"]
}

# firewall http https
resource "google_compute_firewall" "firewall_http_prod" {
  name    = "allow-http-https-prod"
  network = google_compute_network.network_prod.self_link

  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["server-prod"]
}
