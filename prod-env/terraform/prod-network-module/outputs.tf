output "network_id_prod" {
  value = google_compute_network.network_prod.id
}

output "subnetwork_id_prod" {
  value = google_compute_subnetwork.subnetwork_prod.id
}
