output "cluster_user" {
  value = var.user
}

output "prod_cluster_internal_ip" {
  value = module.prod_vm.prod_cluster_internal_ip
}

output "prod_cluster_external_ip" {
  value = module.prod_vm.prod_cluster_external_ip
}
